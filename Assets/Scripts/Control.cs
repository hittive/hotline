﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Rigidbody))]
public class Control : MonoBehaviour
{

    private float speed =   10f;
    private float angle;

    public float Get_angle()
    {
        return angle;
    }

    void Start()
    {
        
    }
    
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += new Vector3(0.0f, 5 * Time.deltaTime, 0.0f);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position -= new Vector3(0.0f, 5  * Time.deltaTime, 0.0f);
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.position -= new Vector3(5 * Time.deltaTime, 0.0f, 0.0f);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += new Vector3(5 * Time.deltaTime, 0.0f, 0.0f);
        }


        Set_rotation();


}

    void Set_rotation()
    {
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, speed * 50 * Time.deltaTime);
    }
}