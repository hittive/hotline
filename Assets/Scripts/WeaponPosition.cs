﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WeaponPosition : MonoBehaviour {

    private bool is_taken = false;
    private bool go_away = false;
    private float throw_timer = 0.45f;
    private float static_x_pos, static_y_pos;
    private float time_between_shoot = 0.1f;
    private float angle_go_away = 0f;
    public GameObject thePrefab;



    // Use this for initialization
    void Start () {


    }

    // Update is called once per frame
    void Update() {


        if (is_taken == true)
        {
            transform.rotation = GameObject.Find("Hero").GetComponent<Control>().transform.rotation;
            float weapon_angle = GameObject.Find("Hero").GetComponent<Control>().Get_angle();

            float x_post = Mathf.Cos(Mathf.PI * (weapon_angle+180) / 180.0f) / 1.5f;
            float y_post = Mathf.Sin(Mathf.PI * (weapon_angle+180) / 180.0f) / 1.5f;

            transform.position = GameObject.Find("Hero").transform.position + new Vector3(-x_post, -y_post, 0);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            if (is_taken == true)
            {
                is_taken = false;
            }
            else if (Vector3.Distance(transform.position, GameObject.Find("Hero").GetComponent<Control>().transform.position) < 1.0f)
            {
                is_taken = true;
            };

        }

        if (Input.GetMouseButton(1) && is_taken == true) // throw weapon
        {
            float weapon_angle = GameObject.Find("Hero").GetComponent<Control>().Get_angle();

             static_x_pos = Mathf.Cos(Mathf.PI * weapon_angle / 180.0f) / 1.5f;
             static_y_pos = Mathf.Sin(Mathf.PI * weapon_angle / 180.0f) / 1.5f;
            
            is_taken = false;
            go_away = true;
            throw_timer = 0.45f;
        }

        if (Input.GetMouseButtonDown(0) && is_taken == true && time_between_shoot <= 0) // shoot
        {
            time_between_shoot = 0.5f;
            GameObject instance = Instantiate(thePrefab, transform.position, transform.rotation);
            Destroy(instance, 5);
        }

        if (go_away == true) 
        {
            transform.position += new Vector3(-static_y_pos*100f* throw_timer * Time.deltaTime, static_x_pos*100f * throw_timer* Time.deltaTime, 0);

            Quaternion rotation = Quaternion.AngleAxis(angle_go_away+=100.0f * throw_timer, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 0.5f * 50 * Time.deltaTime);

            if (throw_timer <= 0)
            {
                go_away = false;
            }
        }

        Debug.Log(static_x_pos);
        Time_counter();
    }

    void Time_counter()
    {
       if(throw_timer>0) throw_timer -= Time.deltaTime;
       if(time_between_shoot > 0) time_between_shoot -= Time.deltaTime;
    }
}
