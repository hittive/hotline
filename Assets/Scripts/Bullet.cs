﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    float x_post, y_post;


    // Use this for initialization
    void Start ()
    { 
     transform.rotation = GameObject.Find("Hero").GetComponent<Control>().transform.rotation;
     float weapon_angle = GameObject.Find("Hero").GetComponent<Control>().Get_angle();
     x_post = Mathf.Cos(Mathf.PI * weapon_angle / 180.0f) / 1.5f;
     y_post = Mathf.Sin(Mathf.PI * weapon_angle / 180.0f) / 1.5f;
   //  Destroy(this, 0.2f);
    }
	
	// Update is called once per frame
	void Update () {

        transform.position += new Vector3(-y_post * 90f * Time.deltaTime, x_post * 90f  * Time.deltaTime, 0);
     



    }
}
